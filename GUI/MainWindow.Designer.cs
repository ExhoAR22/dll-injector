﻿namespace GUI
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblPID = new System.Windows.Forms.Label();
            this.btnThreadState = new System.Windows.Forms.Button();
            this.btnDetach = new System.Windows.Forms.Button();
            this.pboxIcon = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnStackRemap = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.searchDataTb = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.rawRBtn = new System.Windows.Forms.RadioButton();
            this.asciiRBtn = new System.Windows.Forms.RadioButton();
            this.intRBtn = new System.Windows.Forms.RadioButton();
            this.uintRBtn = new System.Windows.Forms.RadioButton();
            this.segComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnModify = new System.Windows.Forms.Button();
            this.offsetTb = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.newValueTb = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.mintRBtn = new System.Windows.Forms.RadioButton();
            this.mrawRBtn = new System.Windows.Forms.RadioButton();
            this.muintRBtn = new System.Windows.Forms.RadioButton();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.offsetLst = new System.Windows.Forms.ListView();
            this.offsetHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.mASCIIRBtn = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pboxIcon)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblPID);
            this.groupBox1.Controls.Add(this.btnThreadState);
            this.groupBox1.Controls.Add(this.btnDetach);
            this.groupBox1.Controls.Add(this.pboxIcon);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 426);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Info";
            // 
            // lblPID
            // 
            this.lblPID.AutoSize = true;
            this.lblPID.Location = new System.Drawing.Point(91, 153);
            this.lblPID.Name = "lblPID";
            this.lblPID.Size = new System.Drawing.Size(25, 13);
            this.lblPID.TabIndex = 5;
            this.lblPID.Text = "PID";
            // 
            // btnThreadState
            // 
            this.btnThreadState.Location = new System.Drawing.Point(65, 279);
            this.btnThreadState.Name = "btnThreadState";
            this.btnThreadState.Size = new System.Drawing.Size(75, 23);
            this.btnThreadState.TabIndex = 4;
            this.btnThreadState.Text = "Pause";
            this.btnThreadState.UseVisualStyleBackColor = true;
            this.btnThreadState.Click += new System.EventHandler(this.btnThreadState_Click);
            // 
            // btnDetach
            // 
            this.btnDetach.Location = new System.Drawing.Point(65, 250);
            this.btnDetach.Name = "btnDetach";
            this.btnDetach.Size = new System.Drawing.Size(75, 23);
            this.btnDetach.TabIndex = 2;
            this.btnDetach.Text = "Detach";
            this.btnDetach.UseVisualStyleBackColor = true;
            this.btnDetach.Click += new System.EventHandler(this.btnDetach_Click);
            // 
            // pboxIcon
            // 
            this.pboxIcon.Location = new System.Drawing.Point(53, 46);
            this.pboxIcon.Name = "pboxIcon";
            this.pboxIcon.Size = new System.Drawing.Size(100, 100);
            this.pboxIcon.TabIndex = 1;
            this.pboxIcon.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(77, 95);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "No Icon";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnStackRemap);
            this.groupBox2.Controls.Add(this.btnSearch);
            this.groupBox2.Controls.Add(this.searchDataTb);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.rawRBtn);
            this.groupBox2.Controls.Add(this.asciiRBtn);
            this.groupBox2.Controls.Add(this.intRBtn);
            this.groupBox2.Controls.Add(this.uintRBtn);
            this.groupBox2.Controls.Add(this.segComboBox);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(218, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(570, 240);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Search for offsets:";
            // 
            // btnStackRemap
            // 
            this.btnStackRemap.Location = new System.Drawing.Point(374, 23);
            this.btnStackRemap.Name = "btnStackRemap";
            this.btnStackRemap.Size = new System.Drawing.Size(93, 23);
            this.btnStackRemap.TabIndex = 9;
            this.btnStackRemap.Text = "Remap Stacks";
            this.btnStackRemap.UseVisualStyleBackColor = true;
            this.btnStackRemap.Click += new System.EventHandler(this.btnStackRemap_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(473, 23);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 8;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // searchDataTb
            // 
            this.searchDataTb.Location = new System.Drawing.Point(21, 120);
            this.searchDataTb.Multiline = true;
            this.searchDataTb.Name = "searchDataTb";
            this.searchDataTb.Size = new System.Drawing.Size(527, 100);
            this.searchDataTb.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "The value:";
            // 
            // rawRBtn
            // 
            this.rawRBtn.AutoSize = true;
            this.rawRBtn.Location = new System.Drawing.Point(273, 61);
            this.rawRBtn.Name = "rawRBtn";
            this.rawRBtn.Size = new System.Drawing.Size(129, 17);
            this.rawRBtn.TabIndex = 5;
            this.rawRBtn.TabStop = true;
            this.rawRBtn.Text = "Raw Bytes (Split by \',\')";
            this.rawRBtn.UseVisualStyleBackColor = true;
            // 
            // asciiRBtn
            // 
            this.asciiRBtn.AutoSize = true;
            this.asciiRBtn.Location = new System.Drawing.Point(191, 61);
            this.asciiRBtn.Name = "asciiRBtn";
            this.asciiRBtn.Size = new System.Drawing.Size(76, 17);
            this.asciiRBtn.TabIndex = 4;
            this.asciiRBtn.TabStop = true;
            this.asciiRBtn.Text = "ASCII Text";
            this.asciiRBtn.UseVisualStyleBackColor = true;
            // 
            // intRBtn
            // 
            this.intRBtn.AutoSize = true;
            this.intRBtn.Location = new System.Drawing.Point(85, 61);
            this.intRBtn.Name = "intRBtn";
            this.intRBtn.Size = new System.Drawing.Size(100, 17);
            this.intRBtn.TabIndex = 3;
            this.intRBtn.TabStop = true;
            this.intRBtn.Text = "Integer (Signed)";
            this.intRBtn.UseVisualStyleBackColor = true;
            // 
            // uintRBtn
            // 
            this.uintRBtn.AutoSize = true;
            this.uintRBtn.Location = new System.Drawing.Point(21, 61);
            this.uintRBtn.Name = "uintRBtn";
            this.uintRBtn.Size = new System.Drawing.Size(58, 17);
            this.uintRBtn.TabIndex = 2;
            this.uintRBtn.TabStop = true;
            this.uintRBtn.Text = "Integer";
            this.uintRBtn.UseVisualStyleBackColor = true;
            // 
            // segComboBox
            // 
            this.segComboBox.FormattingEnabled = true;
            this.segComboBox.Location = new System.Drawing.Point(97, 25);
            this.segComboBox.Name = "segComboBox";
            this.segComboBox.Size = new System.Drawing.Size(121, 21);
            this.segComboBox.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "The segment:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnModify);
            this.groupBox3.Controls.Add(this.offsetTb);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.newValueTb);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.mintRBtn);
            this.groupBox3.Controls.Add(this.mrawRBtn);
            this.groupBox3.Controls.Add(this.muintRBtn);
            this.groupBox3.Controls.Add(mASCIIRBtn);
            this.groupBox3.Location = new System.Drawing.Point(219, 259);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(569, 179);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Modify values:";
            // 
            // btnModify
            // 
            this.btnModify.Location = new System.Drawing.Point(472, 24);
            this.btnModify.Name = "btnModify";
            this.btnModify.Size = new System.Drawing.Size(75, 23);
            this.btnModify.TabIndex = 9;
            this.btnModify.Text = "Modify";
            this.btnModify.UseVisualStyleBackColor = true;
            this.btnModify.Click += new System.EventHandler(this.btnModify_Click);
            // 
            // offsetTb
            // 
            this.offsetTb.Location = new System.Drawing.Point(81, 26);
            this.offsetTb.Name = "offsetTb";
            this.offsetTb.Size = new System.Drawing.Size(136, 20);
            this.offsetTb.TabIndex = 16;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "The offset:";
            // 
            // newValueTb
            // 
            this.newValueTb.Location = new System.Drawing.Point(20, 111);
            this.newValueTb.Multiline = true;
            this.newValueTb.Name = "newValueTb";
            this.newValueTb.Size = new System.Drawing.Size(527, 52);
            this.newValueTb.TabIndex = 14;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 85);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "The new value:";
            // 
            // mintRBtn
            // 
            this.mintRBtn.AutoSize = true;
            this.mintRBtn.Location = new System.Drawing.Point(20, 59);
            this.mintRBtn.Name = "mintRBtn";
            this.mintRBtn.Size = new System.Drawing.Size(58, 17);
            this.mintRBtn.TabIndex = 9;
            this.mintRBtn.TabStop = true;
            this.mintRBtn.Text = "Integer";
            this.mintRBtn.UseVisualStyleBackColor = true;
            // 
            // mrawRBtn
            // 
            this.mrawRBtn.AutoSize = true;
            this.mrawRBtn.Location = new System.Drawing.Point(272, 59);
            this.mrawRBtn.Name = "mrawRBtn";
            this.mrawRBtn.Size = new System.Drawing.Size(129, 17);
            this.mrawRBtn.TabIndex = 12;
            this.mrawRBtn.TabStop = true;
            this.mrawRBtn.Text = "Raw Bytes (Split by \',\')";
            this.mrawRBtn.UseVisualStyleBackColor = true;
            // 
            // muintRBtn
            // 
            this.muintRBtn.AutoSize = true;
            this.muintRBtn.Location = new System.Drawing.Point(84, 59);
            this.muintRBtn.Name = "muintRBtn";
            this.muintRBtn.Size = new System.Drawing.Size(100, 17);
            this.muintRBtn.TabIndex = 10;
            this.muintRBtn.TabStop = true;
            this.muintRBtn.Text = "Integer (Signed)";
            this.muintRBtn.UseVisualStyleBackColor = true;
            // 
            // mASCIIRBtn
            // 
            mASCIIRBtn.AutoSize = true;
            mASCIIRBtn.Location = new System.Drawing.Point(190, 59);
            mASCIIRBtn.Name = "mASCIIRBtn";
            mASCIIRBtn.Size = new System.Drawing.Size(76, 17);
            mASCIIRBtn.TabIndex = 11;
            mASCIIRBtn.TabStop = true;
            mASCIIRBtn.Text = "ASCII Text";
            mASCIIRBtn.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.offsetLst);
            this.groupBox4.Location = new System.Drawing.Point(795, 13);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(262, 425);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Offsets found:";
            // 
            // offsetLst
            // 
            this.offsetLst.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.offsetHeader});
            this.offsetLst.Location = new System.Drawing.Point(7, 20);
            this.offsetLst.Name = "offsetLst";
            this.offsetLst.Size = new System.Drawing.Size(249, 399);
            this.offsetLst.TabIndex = 0;
            this.offsetLst.UseCompatibleStateImageBehavior = false;
            this.offsetLst.View = System.Windows.Forms.View.Details;
            this.offsetLst.DoubleClick += new System.EventHandler(this.offsetLst_DoubleClick);
            // 
            // offsetHeader
            // 
            this.offsetHeader.Text = "Offset";
            this.offsetHeader.Width = 162;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1069, 450);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "MainWindow";
            this.ShowIcon = false;
            this.Text = "DLLInjector";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainWindow_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pboxIcon)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblPID;
        private System.Windows.Forms.Button btnThreadState;
        private System.Windows.Forms.Button btnDetach;
        private System.Windows.Forms.PictureBox pboxIcon;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox searchDataTb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton rawRBtn;
        private System.Windows.Forms.RadioButton asciiRBtn;
        private System.Windows.Forms.RadioButton intRBtn;
        private System.Windows.Forms.RadioButton uintRBtn;
        private System.Windows.Forms.ComboBox segComboBox;
        private System.Windows.Forms.TextBox newValueTb;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton mintRBtn;
        private System.Windows.Forms.RadioButton mrawRBtn;
        private System.Windows.Forms.RadioButton muintRBtn;
        private System.Windows.Forms.RadioButton mASCIIRBtn;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnModify;
        private System.Windows.Forms.TextBox offsetTb;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnStackRemap;
        private System.Windows.Forms.ListView offsetLst;
        private System.Windows.Forms.ColumnHeader offsetHeader;
    }
}