﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace DLLInjector.API
{
    public static class DLL
    {
        private static Socket Server = null, Connection = null;

        public static bool IsAvailable() // Can we even work?
        {
            return File.Exists(Globals.CD + "\\DLL.dll");
        }

        internal static void WaitForConnection() // Don't invoke from the gui, here for internal useage.
        {
            Server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            Server.Bind(new IPEndPoint(IPAddress.Parse("127.0.0.1"), 1927));
            Server.Listen(1);
            Connection = Server.Accept();
        }

        public static void End() // End the connection. Use this when closing the gui.
        {
            try
            {
                Connection.SendString("end");
                Connection.Close();
                Server.Close();
            }
            catch (ObjectDisposedException e) { }
        }

        /*
         * Pause/Resume the process, basically.
         * ThreadState can either be ThreadState.Asleep or ThreadState.Awake.
         */
        public static void SetThreadState(ThreadState state)
        {
            string command = "set_thread_state " + (state == ThreadState.Awake ? "AWAKE" : "ASLEEP");
            Connection.SendString(command);
            string output = Connection.ReceiveString(1024);
            if (int.Parse(output) != 1)
            {
                throw new CommandExecutionFailedException(command, output);
            }
        }

        /*
         * Searches a segment.
         * callback - A class implementing DLLFoundOffsetCallback. OnOffsetFound will be invoked when an offset is found.
         * OnSearchEnded will be invoked once the search has ended.
         * values - A byte sequence to search for. IMPORTANT! integers should be LITTLE ENDIAN!!!!!!!!!!
         * segment - the segment to search for. Integers will typically reside in ".data", strings will usually be in ".rsrc".
         */
        public static void SearchSegment(DLLFoundOffsetCallback callback, byte[] values, string segment = ".data")
        {
            string command = "search_segment ";
            foreach (byte b in values)
            {
                command += b.ToString() + ",";
            }
            command = command.Substring(0, command.Length - 1) + " " + segment;
            Connection.SendString(command);
            uint count = 0;
            byte[] buff = new byte[4];
            Connection.Receive(buff);
            uint temp = BitConverter.ToUInt32(buff, 0);
            while (temp != 0xffffffff)
            {
                callback.OnOffsetFound(temp);
                count++;
                Connection.Receive(buff);
                temp = BitConverter.ToUInt32(buff, 0);
            }
            callback.OnSearchEnded(count);
        }

        /*
         * Enables or disables console logging. Console logging writes stuff to an active console window of the target.
         * Might make output inconsistent.
         */ 
        public static void SetConsoleLogEnabled(bool enabled)
        {
            Connection.SendString("set_console_log " + (enabled ? "ENABLED" : "DISABLED"));
            string output = Connection.ReceiveString(1024);
            if (int.Parse(output) != 1)
            {
                throw new CommandExecutionFailedException("set_console_log " + (enabled ? "ENABLED" : "DISABLED"), output);
            }
        }

        /*
         * Change a block of memory at a known offset.
         * values - The new byte sequence to put in offset.
         * offset - Where?
         * suspend - Should the threads be suspended to avoid problems?
         */
        public static void Modify(byte[] values, uint offset, bool suspend = true)
        {
            string command = "modify " + offset + " ";
            foreach (byte b in values)
            {
                command += b.ToString() + ",";
            }
            command = command.Substring(0, command.Length - 1);
            Connection.SendString(command);
            if (suspend)
            {
                command += " suspend";
            }
            string output = Connection.ReceiveString(1024);
            if (int.Parse(output) != 1)
            {
                throw new CommandExecutionFailedException(command, output);
            }
        }

        public static List<string> SegmentList()
        {
            Connection.SendString("segment_list");
            return new List<string>(Connection.ReceiveString(2048).Split('\n'));
        }

        public static uint RemapStacks()
        {
            Connection.SendString("remap_stacks");
            byte[] buff = new byte[4];
            Connection.Receive(buff);
            return BitConverter.ToUInt32(buff, 0);
        } 
    }
}
