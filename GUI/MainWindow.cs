﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;
using DLLInjector.API;

namespace GUI
{
    public partial class MainWindow : Form, DLLInjector.API.DLLFoundOffsetCallback
    {
        private Process Target;

        private void RefreshSegmentList()
        {
            segComboBox.Items.Clear();
            foreach (string seg in DLL.SegmentList())
            {
                segComboBox.Items.Add(seg);
            }
        }

        public MainWindow(int pid)
        {
            InitializeComponent();
            try
            {
                Target = Process.GetProcessById(pid);
                lblPID.Text = Target.Id.ToString();
                RefreshSegmentList();
            }
            catch (Exception ex)
            {
                ProcessList.Panic(ex);
            }
        }

        private void btnDetach_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Are you sure? Doing so will exit DLLInjector.", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    DLL.End();
                    Application.Exit();
                }
            }
            catch (Exception ex)
            {
                ProcessList.Panic(ex);
            }
        }

        private void btnThreadState_Click(object sender, EventArgs e)
        {
            try
            {
                DLLInjector.API.ThreadState next = (btnThreadState.Text == "Pause") ? DLLInjector.API.ThreadState.Asleep : DLLInjector.API.ThreadState.Awake;
                DLL.SetThreadState(next);
                btnThreadState.Text = next == DLLInjector.API.ThreadState.Asleep ? "Resume" : "Pause";
            }
            catch (Exception ex)
            {
                ProcessList.Panic(ex);
            }
        }

        private void MainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            DLL.End();
            Application.Exit();
        }

        private void btnStackRemap_Click(object sender, EventArgs e)
        {
            DLL.RemapStacks();
            RefreshSegmentList();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            offsetLst.Items.Clear();
            byte[] rawData = null;
            if (uintRBtn.Checked)
            {
                rawData = BitConverter.GetBytes(UInt32.Parse(searchDataTb.Text));
            }
            else if (intRBtn.Checked)
            {
                rawData = BitConverter.GetBytes(Int32.Parse(searchDataTb.Text));
            }
            else if (asciiRBtn.Checked)
            {
                rawData = Encoding.ASCII.GetBytes(searchDataTb.Text);
            }
            else if (rawRBtn.Checked)
            {
                string[] temp = searchDataTb.Text.Split(',');
                rawData = new byte[temp.Length];
                for (int i = 0; i < temp.Length; i++)
                {
                    rawData[i] = byte.Parse(temp[i]);
                }
            }
            DLL.SearchSegment(this, rawData, segComboBox.SelectedItem.ToString());
        }

        public void OnOffsetFound(uint offset)
        {
            offsetLst.Items.Add(new ListViewItem(offset.ToString()));
            offsetLst.Refresh();
        }

        public void OnSearchEnded(uint count)
        {
            
        }

        private void offsetLst_DoubleClick(object sender, EventArgs e)
        {
            Clipboard.SetText(offsetLst.SelectedItems[0].Text);
            MessageBox.Show("Offset " + offsetLst.SelectedItems[0].Text + " copied!", "Offset copied!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnModify_Click(object sender, EventArgs e)
        {
            byte[] rawData = null;
            if (muintRBtn.Checked)
            {
                rawData = BitConverter.GetBytes(UInt32.Parse(newValueTb.Text));
            }
            else if (mintRBtn.Checked)
            {
                rawData = BitConverter.GetBytes(Int32.Parse(newValueTb.Text));
            }
            else if (mASCIIRBtn.Checked)
            {
                rawData = Encoding.ASCII.GetBytes(newValueTb.Text);
            }
            else if (mrawRBtn.Checked)
            {
                string[] temp = newValueTb.Text.Split(',');
                rawData = new byte[temp.Length];
                for (int i = 0; i < temp.Length; i++)
                {
                    rawData[i] = byte.Parse(temp[i]);
                }
            }
            try
            {
                DLL.Modify(rawData, UInt32.Parse(offsetTb.Text));
            }
            catch (CommandExecutionFailedException cefe)
            {
                ProcessList.Panic(cefe);
            }
        }
    }
}
