#pragma once
#include <string>
#include <winsock2.h>
#include <Windows.h>
#include <ws2tcpip.h>
#include <iphlpapi.h>
#include <mstcpip.h>
#include <vector>
#pragma comment(lib, "ws2_32")

class NetworkingUtility
{
public:
	static bool StartUp();
	static std::vector<std::string> GetLocalIPv4Addresses();
	static bool CleanUp();
	static bool IsUseable();
	static std::string GetLastNetworkingErrorString();
protected:
private:
	static bool Useable;
};

class TCPSocket
{
public:
	TCPSocket();
	bool Send(std::string data, int size = 0); // [Blocking]
	std::string Receive(int max = 1024); // [Blocking]
	std::string ReceiveUntil(char s); // [Blocking]
	bool Close();
	bool Connect(std::string ip, unsigned short port);
	bool SendBytes(void* buff, uint32_t count);
protected:
private:
	TCPSocket(SOCKET client);
	SOCKET ws;
	addrinfo *result = NULL, hints;
};