﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;
using DLLInjector.API;

namespace GUI
{
    public partial class ProcessList : Form
    {
        private void RefreshList()
        {
            plist.Items.Clear();
            Process[] p = Process.GetProcesses();
            foreach (Process c in p)
            {
                if (filter.Text == "" || c.ProcessName.Contains(filter.Text))
                {
                    plist.Items.Add(new ListViewItem(new string[] { c.ProcessName, c.Id.ToString() }));
                }
            }
        }

        public static void Panic(Exception e)
        {
            MessageBox.Show("A fatal error occured!\n\n.NET Exception: " + e.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            Application.Exit();
        }

        public ProcessList()
        {
            InitializeComponent();
            RefreshList();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            RefreshList();
        }

        private void plist_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                Injector.Inject(int.Parse(plist.SelectedItems[0].SubItems[1].Text));
                new MainWindow(int.Parse(plist.SelectedItems[0].SubItems[1].Text)).Show();
                this.Hide();
            }
            catch (Exception ex)
            {
                Panic(ex);
            } 
        }
    }
}
