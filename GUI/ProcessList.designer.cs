﻿namespace GUI
{
    partial class ProcessList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.plist = new System.Windows.Forms.ListView();
            this.PNAME = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.PID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label1 = new System.Windows.Forms.Label();
            this.filter = new System.Windows.Forms.TextBox();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // plist
            // 
            this.plist.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.PNAME,
            this.PID});
            this.plist.Location = new System.Drawing.Point(12, 53);
            this.plist.Name = "plist";
            this.plist.Size = new System.Drawing.Size(376, 401);
            this.plist.TabIndex = 0;
            this.plist.UseCompatibleStateImageBehavior = false;
            this.plist.View = System.Windows.Forms.View.Details;
            this.plist.DoubleClick += new System.EventHandler(this.plist_DoubleClick);
            // 
            // PNAME
            // 
            this.PNAME.Text = "Name";
            this.PNAME.Width = 330;
            // 
            // PID
            // 
            this.PID.Text = "PID";
            this.PID.Width = 102;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 465);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Filter:";
            // 
            // filter
            // 
            this.filter.Location = new System.Drawing.Point(51, 463);
            this.filter.Name = "filter";
            this.filter.Size = new System.Drawing.Size(258, 20);
            this.filter.TabIndex = 2;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(315, 460);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 23);
            this.btnRefresh.TabIndex = 3;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(375, 26);
            this.label2.TabIndex = 4;
            this.label2.Text = "Welcome to DLLInjector! Please select a process to attach to, so we can start\r\nwo" +
    "rking!";
            // 
            // ProcessList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(398, 491);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.filter);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.plist);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "ProcessList";
            this.ShowIcon = false;
            this.Text = "DLLInjector";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView plist;
        private System.Windows.Forms.ColumnHeader PNAME;
        private System.Windows.Forms.ColumnHeader PID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox filter;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Label label2;
    }
}

