# DLLInjector
A DLL injector bundled with a premade DLL and a target for testing. This dll can scan the target's PE segments (.data, .text, .rsrc, etc..) for byte sequences, modify byte sequences given the offset, and break/resmue execution.

***Important! THIS IS FOR EDUCATIONAL PURPOSES ONLY! I AM NOT RESPONSIBLE FOR ANY DAMAGE THIS MAY CAUSE! DO NOT USE IT TO DO HARM!***

## Notes
* An API for communication with the bundled DLL is also provided for .NET applications. The API includes functions for the capabilities below.
* The project is built with only 32-bit applications in mind. Slight modifications are required for 64-bit support.

## Usage, If someone actually will (which I doubt)
### Injecting a DLL
```
injector <pid> <path_to_dll|--stddll>
```

### Communicating with the bundled DLL
* A TCP socket must be accepting on 127.0.0.1:1927 before injection.

**Searching for byte sequences**
* Send this through the accepted socket: search_segment <bytes_seperated_by_','_in_decimal> <segment_name>
* recv from the socket until you get 0xffffffff.

**Modifying byte sequences**
* Send this through the accepted socket: modify <offset_in_decimal> <new_bytes_seperated_by_','_in_decimal>
* Expect to recv "1".

**Breaking/resuming execution**
* Send this through the accepted socket: set_thread_state ASLEEP|AWAKE
* Expect to recv "1".

**Getting a list of segments**
* Send this through the accepted socket: segment_list
* Expect to recv a string containing the segment names seperated by '\n'.

**Remapping stacks (To deal with thread creation/termination)**
* Send this through the accepted socket: remap_stacks
* Expect to recv the number of stacks mapped (little endian).
