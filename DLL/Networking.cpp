#include "Networking.h"

using namespace std;

bool NetworkingUtility::Useable = false;

bool NetworkingUtility::StartUp()
{
	WSADATA WSData;
	return Useable = !WSAStartup(MAKEWORD(2, 2), &WSData); // May get WSAEINPROGRESS if already initialized.
};

vector<string> NetworkingUtility::GetLocalIPv4Addresses()
{
	vector<string> res;
	char szBuffer[1024];
	if (gethostname(szBuffer, sizeof(szBuffer)) == SOCKET_ERROR) { return res; }
	hostent* host = gethostbyname(szBuffer);
	if (!host) { return res; }
	for (int i = 0; host->h_addr_list[i]; i++)
	{
		string r = to_string(((int)((in_addr*)(host->h_addr_list[i]))->S_un.S_un_b.s_b1)) + ".";
		r += to_string(((int)((in_addr*)(host->h_addr_list[i]))->S_un.S_un_b.s_b2)) + ".";
		r += to_string(((int)((in_addr*)(host->h_addr_list[i]))->S_un.S_un_b.s_b3)) + ".";
		r += to_string(((int)((in_addr*)(host->h_addr_list[i]))->S_un.S_un_b.s_b4));
		res.push_back(r);
	}
	return res;
};

bool NetworkingUtility::CleanUp()
{
	Useable = false;
	return WSACleanup();
};

bool NetworkingUtility::IsUseable()
{
	return Useable;
};

string NetworkingUtility::GetLastNetworkingErrorString()
{
	unsigned long errorMessageID = WSAGetLastError();
	if (errorMessageID == 0) { return ""; }
	LPSTR messageBuffer = NULL;
	size_t size = FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL, errorMessageID, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&messageBuffer, 0, NULL);
	string message(messageBuffer, size);
	LocalFree(messageBuffer);
	return message;
};

TCPSocket::TCPSocket(SOCKET client)
{
	this->ws = client;
}

TCPSocket::TCPSocket()
{
	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = NULL;
};

bool TCPSocket::Send(string data, int size)
{
	WSASetLastError(0);
	return send(ws, data.c_str(), (size ? size : data.length()), 0); // '\0'
};

bool TCPSocket::SendBytes(void* data, uint32_t size)
{
	WSASetLastError(0);
	return send(ws, reinterpret_cast<char*>(data), size, 0);
};

string TCPSocket::Receive(int max)
{
	WSASetLastError(0);
	char* buffer = new char[max];
	int x = recv(ws, buffer, max, 0);
	if (x < 0 || WSAGetLastError())
	{
		delete[] buffer;
		return "";
	}
	buffer[x] = 0;
	string res(buffer);
	delete[] buffer;
	return res;
};

bool TCPSocket::Close()
{
	WSASetLastError(0);
	return closesocket(ws);
};

bool TCPSocket::Connect(string ip, unsigned short port)
{
	WSASetLastError(0);
	if (!getaddrinfo(ip.c_str(), to_string(port).c_str(), &hints, &result))
	{
		this->ws = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
		if (ws == INVALID_SOCKET) { return false; }
		if (connect(ws, result->ai_addr, (int)result->ai_addrlen)) { return false; }
		freeaddrinfo(result);
		return true;
	}
	else return false;
};

string TCPSocket::ReceiveUntil(char c) {
	string res;
	char temp = 0;
	auto x = recv(ws, &temp, 1, 0);
	if (x < 0 || WSAGetLastError()) {
		return "";
	}
	while (temp != c) {
		res += temp;
		x = recv(ws, &temp, 1, 0);
		if (x < 0 || WSAGetLastError()) {
			return "";
		}
	}
	return res;
};