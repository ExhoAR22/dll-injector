#include <iostream>
#include <Windows.h>

using namespace std;

volatile unsigned int x = 0xabcdef12;

int main()
{
	volatile unsigned int y = x;
	printf("&x = %x\n&y = %x\n", &x, &y);
	while (y == x);
	cout << "Modified!\n";
	system("pause");
};