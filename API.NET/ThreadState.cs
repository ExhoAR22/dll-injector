﻿namespace DLLInjector.API
{
    public enum ThreadState
    {
        Asleep = 0,
        Awake = 1
    }
}
