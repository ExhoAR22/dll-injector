#include <Windows.h>
#include <string>

using namespace std;

wstring GetFullPathW(wstring path, unsigned int maxChar = 4096)
{
	wchar_t* c = new wchar_t[maxChar]{ 0 };
	GetFullPathNameW(path.c_str(), maxChar, c, NULL);
	wstring res = c;
	delete[] c;
	return res;
};

bool LoadLibraryInjectDLL(wstring path, DWORD pid, bool wait = false)
{
	auto handle = OpenProcess(PROCESS_CREATE_THREAD | PROCESS_QUERY_INFORMATION | PROCESS_VM_OPERATION | PROCESS_VM_WRITE | PROCESS_VM_READ, false, pid);
	if (handle == INVALID_HANDLE_VALUE || !handle) { return false; }
	wstring dllPath = GetFullPathW(path);
	auto dllPathAddr = VirtualAllocEx(handle, 0, dllPath.length() * 2, MEM_RESERVE | MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	WriteProcessMemory(handle, dllPathAddr, dllPath.c_str(), dllPath.length() * 2, NULL);
	auto loadLibAddr = GetProcAddress(GetModuleHandleA("kernel32.dll"), "LoadLibraryW");
	auto rThread = CreateRemoteThread(handle, NULL, 0, (LPTHREAD_START_ROUTINE)loadLibAddr, dllPathAddr, 0, NULL);
	if (!wait) { return true; }
	WaitForSingleObject(rThread, INFINITE);
	CloseHandle(rThread);
	CloseHandle(handle);
	return true;
};

int _stdcall wWinMain(HINSTANCE hInst, HINSTANCE hPInst, wchar_t* lpCmdLine, int nCmdShow)
{
	wstring pid, path;
	int i = 0;
	while (lpCmdLine[i] != ' ') { pid += lpCmdLine[i++]; }
	i++;
	while (lpCmdLine[i]) { path += lpCmdLine[i++]; }
	if (path == L"--stddll") { path = L"DLL.dll"; }
	return !LoadLibraryInjectDLL(GetFullPathW(path), stoi(pid));
};
