﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DLLInjector.API
{
    public class InvalidPIDException : Exception
    {
        private string mn = "";
        public InvalidPIDException(int pid) { mn = "The provided PID (" + pid + ") is invalid. No process has such a PID."; }
        public override string Message => mn;
    }
}
