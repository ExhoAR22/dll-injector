﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DLLInjector.API
{
    public interface DLLFoundOffsetCallback
    {
        void OnOffsetFound(uint offset);
        void OnSearchEnded(uint count);
    }
}
