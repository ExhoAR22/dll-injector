#include <string>
#include <fstream>
#include "Networking.h"
#include <DbgHelp.h> // Used to deal with PE stuff
#include <tlhelp32.h>
#include <iostream>
#include <sstream>
#include <map>
#pragma comment(lib, "dbghelp.lib") // As well...

using namespace std;

typedef LONG NTSTATUS;
typedef DWORD KPRIORITY;
typedef WORD UWORD;

typedef struct _CLIENT_ID {
	PVOID UniqueProcess;
	PVOID UniqueThread;
} CLIENT_ID, *PCLIENT_ID;

typedef struct _THREAD_BASIC_INFORMATION {
	NTSTATUS                ExitStatus;
	PVOID                   TebBaseAddress;
	CLIENT_ID               ClientId;
	KAFFINITY               AffinityMask;
	KPRIORITY               Priority;
	KPRIORITY               BasePriority;
} THREAD_BASIC_INFORMATION, *PTHREAD_BASIC_INFORMATION;

enum THREADINFOCLASS {
	ThreadBasicInformation,
};

typedef NTSTATUS(_stdcall *NtQueryInformationThread_ptr)(HANDLE, THREADINFOCLASS, PVOID, ULONG, PULONG);
typedef NTSTATUS(_stdcall *NtReadVirtualMemory_ptr)(HANDLE, PVOID, PVOID, ULONG, PULONG);
typedef NTSTATUS(_stdcall *NtWriteVirtualMemory_ptr)(HANDLE, PVOID, PVOID, ULONG, PULONG);
bool console_log = true; // Should we log to stdout?
#define LOG_FILE_PATH (string(getenv("LOCALAPPDATA")) + "\\DLLInjector\\stddll.log")

void log(string s) { // Used to log stuff... Logs can be to the file the path of which is above, and to stdout.
	ofstream o(LOG_FILE_PATH, fstream::app);
	o << s << endl;
	o.close();
	if (console_log) {
		cout << "mmstddll: " << s << endl;
	}
}

vector<string> split(string src, string delim)
{
	if (src.find(delim) == string::npos) { 
		return vector<string>({ src });
	}
	vector<string> res;
	unsigned int pos = 0;
	string token;
	while ((pos = src.find(delim)) != string::npos) {
		token = src.substr(0, pos);
		res.push_back(token);
		src.erase(0, pos + delim.length());
	}
	res.push_back(src);
	return res;
};

string replace(string original, string olds, string news)
{
	int findindex = 0;
	while ((findindex = original.find(olds, findindex)) != -1)
	{
		original.replace(findindex, olds.length(), news);
		findindex += news.length();
	}
	return original;
};

string to_hex_string(unsigned int x) { // Converts x to a hex string representation.
	stringstream stream;
	stream << "0x" << std::hex << x;
	return stream.str();
}

struct Segment { // The struct we'll use to hold info about PE segments..
	string name; // The name, i.g. ".data", ".rsrc", ".text", etc...
	DWORD virtual_address, size; // Where in the address space does it begin? what's it's size?

	string to_string() { // For convinience
		return name + ": " + to_hex_string(virtual_address) + ", " + to_hex_string(size);
	}
};

// Use with set_thread_state. 
#define ASLEEP 0
#define AWAKE 1

// This function sets the thread state (i.g. asleep or awake)
int set_thread_state(bool action) {
	log("Setting thread state to " + action ? "AWAKE" : "ASLEEP");
	THREADENTRY32 te32; // This will hold info about the enumerated threads.
	HANDLE hThreadSnap = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0); // We'll iterate over this snapshot.
	if (hThreadSnap == INVALID_HANDLE_VALUE) {
		log("CreateToolhelp32Snapshot failed, GetLastError returns " + to_hex_string(GetLastError()) + ".");
		return 0;
	}
	te32.dwSize = sizeof(THREADENTRY32);
	if (!Thread32First(hThreadSnap, &te32)) { // Try to start enumeration.
		CloseHandle(hThreadSnap);
		log("Thread32First failed, GetLastError returns " + to_hex_string(GetLastError()) + ".");
		return 0;
	}
	do {
		if (te32.th32OwnerProcessID == GetCurrentProcessId() && te32.th32ThreadID != GetCurrentThreadId()) { // Does this thread belong to the process? is it *not* us?!
			auto hThread = OpenThread(THREAD_SUSPEND_RESUME, FALSE, te32.th32ThreadID);
			if (action) {
				log("Resuming thread " + to_string(te32.th32ThreadID) + "...");
				ResumeThread(hThread);
			}
			else {
				log("Suspending thread " + to_string(te32.th32ThreadID) + "...");
				SuspendThread(hThread);
			}
			CloseHandle(hThread);
		}
	} while (Thread32Next(hThreadSnap, &te32)); // Keep doing this, yea.
	CloseHandle(hThreadSnap);
	log("Thread state set.");
	return 1;
};

// This function puts info about the stack of each thread into segments.
unsigned int map_stacks(NtQueryInformationThread_ptr NtQueryInformationThread, map<string, Segment>& segments) {
	auto hProcess = GetCurrentProcess();
	unsigned int count = 0;
	log("Mapping stacks...");
	THREADENTRY32 te32; // This will hold info about the enumerated threads.
	HANDLE hThreadSnap = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0); // We'll iterate over this snapshot.
	if (hThreadSnap == INVALID_HANDLE_VALUE) {
		log("CreateToolhelp32Snapshot failed, GetLastError returns " + to_hex_string(GetLastError()) + ".");
		return count;
	}
	te32.dwSize = sizeof(THREADENTRY32);
	if (!Thread32First(hThreadSnap, &te32)) { // Try to start enumeration.
		CloseHandle(hThreadSnap);
		log("Thread32First failed, GetLastError returns " + to_hex_string(GetLastError()) + ".");
		return count;
	}
	do {
		if (te32.th32OwnerProcessID == GetCurrentProcessId() && te32.th32ThreadID != GetCurrentThreadId()) { // Does this thread belong to the process? is it *not* us?!
			count++; // Where there's a thread there's a stack.
			NT_TIB tib = { 0 }; // This struct will contain what we need.
			THREAD_BASIC_INFORMATION tbi = { 0 }; // This will be used to obtain the address passed to ReadProcessMemory.
			auto hThread = OpenThread(THREAD_ALL_ACCESS, FALSE, te32.th32ThreadID);
			NTSTATUS status = NtQueryInformationThread(hThread, ThreadBasicInformation, &tbi, sizeof(tbi), nullptr);
			if (status >= 0) {
				ReadProcessMemory(hProcess, tbi.TebBaseAddress, &tib, sizeof(tbi), nullptr);
				Segment s;
				s.name = "stack" + to_string(count - 1);
				s.virtual_address = (DWORD)tib.StackLimit; // Since stack grows upside down, we put the higher limit in address,
				s.size = (DWORD)tib.StackBase - s.virtual_address; // And this in the size.
				log("Found stack: " + s.to_string());
				segments[s.name] = s;
			}
			CloseHandle(hThread);
		}
	} while (Thread32Next(hThreadSnap, &te32)); // Keep doing this, yea.
	CloseHandle(hThreadSnap);
	log("Stacks mapped.");
	return count;
};

bool in_stack(const map<string, Segment>& segments, DWORD va) {
	for (pair<string, Segment> x : segments) {
		if (x.second.virtual_address <= va && (x.second.virtual_address + x.second.size) > va) {
			return x.second.name.length() > 5 && x.second.name.substr(0, 5) == "stack";
		}
	}
	return false;
}

bool _stdcall DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved) {
	DeleteFileA(LOG_FILE_PATH.c_str()); // Delete old log.
	log("DllMain Invoked.");
	if (fdwReason != DLL_PROCESS_ATTACH) { // DllMain stuff, only run once, yada yada.
		log("fdwReason is not DLL_PROCESS_ATTACH. (fdwReason == " + to_hex_string(fdwReason) + ")");
		log("Returning...");
		return true;
	}
	log("Injection successful. (fdwReason == " + to_hex_string(fdwReason) + ")");

	log("Loading ntdll.dll...");
	auto ntdll = GetModuleHandleA("ntdll.dll");
	bool we_loaded_ntdll = !ntdll;
	if (we_loaded_ntdll)
	{
		log("ntdll not loaded; fixing...");
		ntdll = LoadLibraryA("ntdll.dll");
	}
	NtQueryInformationThread_ptr NtQueryInformationThread = reinterpret_cast<NtQueryInformationThread_ptr>(GetProcAddress(ntdll, "NtQueryInformationThread"));
	NtReadVirtualMemory_ptr NtReadVirtualMemory = reinterpret_cast<NtReadVirtualMemory_ptr>(GetProcAddress(ntdll, "NtReadVirtualMemory"));
	NtWriteVirtualMemory_ptr NtWriteVirtualMemory = reinterpret_cast<NtReadVirtualMemory_ptr>(GetProcAddress(ntdll, "NtWriteVirtualMemory"));
	if (!NtQueryInformationThread || !NtReadVirtualMemory || !NtWriteVirtualMemory) {
		log("Unable to load NtQueryInformationThread or NtReadVirtualMemory, will be unable to work with stacks :(");
	}

	byte* base = (byte*)GetModuleHandleA(NULL); // Where is our executable at?
	log("base == " + to_hex_string((unsigned int)base) + ", Parsing image headers...");
	IMAGE_NT_HEADERS* image_nt_header = ImageNtHeader(GetModuleHandleA(NULL)); // PE Image header?
	IMAGE_SECTION_HEADER* section_header = (IMAGE_SECTION_HEADER*)(image_nt_header + 1); // Start looping over segment headers.
	map<string, Segment> segments;
	log("Image has " + to_string(image_nt_header->FileHeader.NumberOfSections) + " segments.");
	for (int i = 0; i < image_nt_header->FileHeader.NumberOfSections; i++) {
		Segment s;
		s.name = (char*)section_header->Name;
		s.virtual_address = section_header->VirtualAddress;
		s.size = section_header->Misc.VirtualSize;
		segments[s.name] = s; // Put it into our segment list for easy access.
		log("Found segment: " + s.to_string());
		section_header++; // Next!
	}
	unsigned int stack_count = map_stacks(NtQueryInformationThread, segments);
	log("Total stacks found: " + to_string(stack_count));

	TCPSocket gui;
	log("Trying to initialize WSA...");
	bool we_initialized_wsa = NetworkingUtility::StartUp(); // Here's an advanced one... WSA might have already been initialized by the target. If so remember it!
	log(we_initialized_wsa ? "We initialized WSA." : "We didn't initialize WSA.");
	log("Trying to connect to 127.0.0.1:1927...");
	log(gui.Connect("127.0.0.1", 1927) ? "Connected. receiving..." : "Timeout. Loop will break with socket error.");

	while (true) { // Standard loop - recv, handle, send, again till the end.
		string r = gui.ReceiveUntil('\n');

		if (r == "") {
			log("Socket error");
			log("Breaking loop.");
			break;
		}

		log("Received: " + r);
		vector<string> req = split(r, " ");

		if (req[0] == "end") { // end
			log("Breaking loop.");
			break;
		}

		else if (req[0] == "segment_list") {
			string res;
			for (pair<string, Segment> s : segments) {
				res += s.first + "\n";
			}
			gui.Send(res.substr(0, res.length() - 1));
		}

		else if (req[0] == "remap_stacks") {
			unsigned int x = map_stacks(NtQueryInformationThread, segments);
			gui.SendBytes(&x, 4);
		}

		else if (req[0] == "search_segment") { // We were told to do a search... OK. We. can. do. this!
			log("Searching for { " + replace(req[1], ",", ", ") + " } in the " + req[2] + " segment...");
			vector<byte> values;
			for (string v : split(req[1], ",")) { // Parse the array...
				values.push_back(stoi(v));
			}
			Segment segment_to_search;
			for (pair<string, Segment> s : segments) { // find the segment by name
				if (s.first == req[2]) {
					segment_to_search = s.second;
					break;
				}
			}
			if (segment_to_search.name != req[2]) {
				gui.Send("0");
			}
			else {
				byte* buff = 0;
				DWORD real_va;
				log("The requested segment: " + segment_to_search.to_string());
				if (req[2].length() >= 5 && req[2].substr(0, 5) == "stack") {
					buff = new byte[segment_to_search.size];
					ULONG nread = 0;
					log("Calling NtReadVirtualMemory...");
					NtReadVirtualMemory(GetCurrentProcess(), (void*)segment_to_search.virtual_address, buff, segment_to_search.size, &nread);
					log("NtReadVirtualMemory returned!");
					for (DWORD offset = 0; offset < segment_to_search.size; offset++) {
						bool ok = true;
						for (DWORD i = 1; i < values.size(); i++) {
							if (buff[offset + i] != values[i]) {
								ok = false;
								break;
							}
						}
						if (ok) {
							DWORD result = segment_to_search.virtual_address + offset;
							log("Found at " + to_hex_string(result));
							gui.SendBytes(&result, 4);
						}
					}
					delete[] buff;
				}
				else {
					for (DWORD offset = 0; offset < segment_to_search.size; offset++) { // Now search for this exact sequence of bytes.
						if (base[segment_to_search.virtual_address + offset] == values[0]) {
							bool ok = true;
							for (DWORD i = 1; i < values.size(); i++) {
								if (base[segment_to_search.virtual_address + offset + i] != values[i]) {
									ok = false;
									break;
								}
							}
							if (ok) {
								DWORD result = segment_to_search.virtual_address + offset;
								log("Found at " + to_hex_string(result));
								gui.SendBytes(&result, 4);
							}
						}
					}
				}
				DWORD end = 0xffffffff; // Loop ended. Send impossible address.
				gui.SendBytes(&end, 4);
			}
		}

		else if (req[0] == "set_thread_state") {
			gui.Send(to_string(set_thread_state(req[1] == "AWAKE")));
		}

		else if (req[0] == "modify") {
			if (req.size() > 3 && req[3] == "suspend") {
				set_thread_state(ASLEEP);
			}
			DWORD offset = stoi(req[1]);
			vector<byte> values;
			for (string v : split(req[2], ",")) {
				values.push_back(stoi(v));
			}
			if (in_stack(segments, offset)) {
				log("The desired offset is in a stack! Using NtWriteVirtualMemory.");
				ULONG res = 0;
				NtWriteVirtualMemory(GetCurrentProcess(), (PVOID)offset, (PVOID)&values[0], values.size(), &res);
				log("NtWriteVirtualMemory returned!");
			}
			else {
				for (DWORD off = 0; off < values.size(); off++) {
					base[offset + off] = values[off];
				}
			}
			if (req.size() > 3 && req[3] == "suspend") {
				set_thread_state(AWAKE);
			}
			gui.Send("1");
		}

		else if (req[0] == "set_console_log") {
			console_log = req[1] == "ENABLED";
			gui.Send("1");
		}
	}
	log("Closing socket...");
	gui.Close();
	if (we_initialized_wsa) {
		log("Uninitializing WSA...");
		NetworkingUtility::CleanUp();
	}
	if (we_loaded_ntdll) {
		log("We loaded ntdll... so we need to clean up.");
		FreeLibrary(ntdll);
	}
	log("Returning...");
	return true;
};