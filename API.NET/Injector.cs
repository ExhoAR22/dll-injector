﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Diagnostics;
using System.Reflection;

namespace DLLInjector.API
{
    public static class Injector
    {
        public static bool IsAvailable() // Can we even work??
        {
            return File.Exists(Globals.CD + "\\Injector.exe");
        }

        /*
         * Injects to a process.
         * pid - the process' id.
         */
        public static void Inject(int pid)
        {
            if (!IsAvailable()) { throw new ModuleUnavailableException("Injector"); }
            else if (!DLL.IsAvailable()) { throw new ModuleUnavailableException("DLL"); }
            else
            {
                Process process = new Process();
                process.StartInfo.FileName = Globals.CD + "\\Injector.exe";
                process.StartInfo.Arguments = pid + " --stddll";
                process.StartInfo.WorkingDirectory = Globals.CD;
                process.Start();
                process.WaitForExit();
                if (process.ExitCode != 0) { throw new InvalidPIDException(pid); }
                DLL.WaitForConnection();
            }
        }
    }
}
