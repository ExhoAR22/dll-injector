﻿using System;

namespace DLLInjector.API
{
    public class ModuleUnavailableException : Exception
    {
        private string mn = "";
        public ModuleUnavailableException(string s) { mn = "The module \"" + s + "\" is unavailable."; }
        public override string Message => mn;
    }
}
