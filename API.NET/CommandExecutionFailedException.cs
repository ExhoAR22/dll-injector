﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DLLInjector.API
{
    public class CommandExecutionFailedException : Exception
    {
        private string mn = "";
        public CommandExecutionFailedException(string command, string output) { mn = "The command \"" + command + "\" failed to execute. The DLL returned \"" + output + "\"."; }
        public override string Message => mn;
    }
}
