﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Reflection;
using System.Text;

namespace DLLInjector.API
{
    static class Globals
    {
        public static string CD = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);

        public static int SendString(this Socket socket, string s)
        {
            return socket.Send(Encoding.ASCII.GetBytes(s + "\n"));
        }

        public static string ReceiveString(this Socket socket, int count)
        {
            byte[] buff = new byte[count];
            socket.Receive(buff);
            return Encoding.ASCII.GetString(buff);
        }
    }
}
